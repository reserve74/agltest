# README #

**AGL TEST**

A programming challenge for potential AGL workers has been defined.

Reading a JSON-formatted website file, create a list of names of pets,
but only if the pets are cats; then sort the names alphabetically, grouped together
by the gender of their owner.

e.g. the result should appear something like

	Female
	     * Ginger
	     * Max
	     * Rogers
	Male
	     * Roselia
	     * Spot

***About***

This solution is a console application created in C# in Visual Studio. It uses C#'s 
List and LINQ to accomplish the tasks. The formatting of the output to suit different 
display, web sites, or mobile devices, would be a matter of deploying the appropriate 
UI code. Reading the data and manipulating the data is what the coding challenge 
reads as being focussed on, which is why a console app was selected.

***Description***

It consists of two main code files, one containing the object
classes (agltestclases.cs): Person, Pet, and an object created to hold
the grouped petOwnerGender and petName.
The Person object includes one or more Pets.
This other file, Program.cs, contains the Main() execution method.

So 

* read the JSON
* create the System.Collections.Generic.List of the <Person> object.
	* including the null case, otherwise one or more Pets that the Person may have
* use LINQ queries to manipulate the data & create the new data objects
* use another LINQ query to order that set of data objects
* create a simple CONSOLE output of the ordered data
