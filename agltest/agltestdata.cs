﻿using System;
using System.Collections.Generic;

namespace agltest
{
    public class agltesting
    {
        // A place to put various test data, test functions

        public static void agltestdata()
        {
			Person phil = new Person("Phil", "Male", 34, new List<Pet> { new Pet("joe", "dog"), new Pet("jim", "cat") });
			Person jack = new Person("Jack", "Intersex", 29, new List<Pet>());
			Person jill = new Person("Jill", "Female", 64, new List<Pet> { new Pet("sam", "cat"), new Pet("alfred", "parrot") });

			List<Person> people = new List<Person> { phil, jack, jill };

			foreach (Person p in people)
			{
				string sexPronoun;
				string descriptionVerb;

				if (p.personGender == "Male") { sexPronoun = "he"; descriptionVerb = "is"; }
				else if (p.personGender == "Female") { sexPronoun = "she"; descriptionVerb = " is "; }
				else { sexPronoun = "they"; descriptionVerb = "are"; }

				Console.WriteLine("\nHere is {0}, {1} {2} {3}, aged {4}.", p.personName, sexPronoun, descriptionVerb, p.personGender, p.personAge);

				if (p.personPetList.Count == 0) Console.Write("{0} has no pets.", p.personName);
				else Console.Write("And this is the list of {0}'s pets:\t", p.personName);

				p.personPetList.ForEach(Pet => Console.Write("{0}\t", Pet.petName + "," + Pet.petType));
			}
		}
    }
}
