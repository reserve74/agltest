﻿using System;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

namespace agltest
{
    //
    // A programming challenge for potential AGL workers has been defined.
    // 
    // Reading a JSON-formatted website file, create a list of pet names,
    // but only if the pets are cats, and sort the names alphabetically, grouped
    // by the gender of their owner.
    //
    // e.g. the result should appear something like
    // Female
    //      * Ginger
    //      * Max
    //      * Rogers
    // Male
    //      * Roselia
    //      * Spot
    //

    // This solution consists of two main code files, one containing the object
    // classes (agltestclases.cs): Person, Pet, and an object created to hold
    // the grouped petOwnerGender and petName.
    // The Person object includes one or more Pets.
    // This present file is the other main file, containing the Main() method.

    // So 
    // * read the JSON
    // * create the System.Collections.Generic.List of the <Person> object.
    // ** including the null, or one or more Pets that the Person may have
    // * use LINQ queries to manipulate the data & create the new data objects
    // * use another LINQ query to order that set of data objects
    // * create a simple CONSOLE output of the ordered data

    class MainClass
    {
        public static void Main(string[] args)
        {
            //agltesting.agltestdata();

            var jsonOutput = new WebClient().DownloadString("http://agl-developer-test.azurewebsites.net/people.json");

            // Debug, verify that we've got proper connection to the site
            //Console.Write(jsonOutput);

            var newPeople = new List<Person>();
            newPeople = (List<Person>)JsonConvert.DeserializeObject(jsonOutput, typeof(List<Person>));

            List<PetOwnerGender> genderCatList = new List<PetOwnerGender>();

            foreach (Person pax in newPeople) 
            {
                //Console.WriteLine("The next person is {0}.", pax.personName);
                //Console.WriteLine("They are {0}.", pax.personGender);
                //Console.WriteLine("And their cats are: ");
                var petQueryList = (from animal in pax.personPetList 
                              where animal.petType.ToLower() == "cat"
                              select animal).Distinct().ToList();
                foreach (Pet p in petQueryList)
                { 
                    genderCatList.Add(new PetOwnerGender(p.petName, pax.personGender)); 
                }
            }

            var resultQuery = 
                from petpog in genderCatList
                group petpog by petpog.petOwnerGender into petgroup
                orderby petgroup.Key // the Key is the petOwnerGender
                select petgroup;

            foreach (var petgroup in resultQuery)
            {
                Console.WriteLine("{0}",petgroup.Key);
                var outputPetOrder = (from PetOwnerGender pog in petgroup
                                           orderby pog.petName
                                           select pog).ToList();
                foreach (PetOwnerGender pog in outputPetOrder)
                {
                    Console.WriteLine("\t * {0}", pog.petName);
                }
            }
            // Keep the console window open in debug mode.
			Console.WriteLine("Press any key to exit.");
			Console.ReadKey();

		}
    }

}
