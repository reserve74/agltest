﻿using System;
using System.Collections.Generic;

namespace agltest
{
	public class Person
	{
		// Properties
		public string personName { get; set; }
		public string personGender { get; set; }
		public int personAge { get; set; }
		public List<Pet> personPetList { get; set; } // a List<> of pets that the person may own

		// Constructor
		public Person(string name, string gender, int age, List<Pet> pets)
		{
			personName = name;
			personGender = gender;
			personAge = age;
			personPetList = new List<Pet>();
			if (pets != null)
			{
				foreach (Pet somePet in pets)
				{
					personPetList.Add(new Pet(somePet.petName, somePet.petType));
				}
			}
		}
	}

	public class Pet
	{
		// Properties
		public string petName { get; set; }
		public string petType { get; set; }

		// some sort of derived property for the owner of the pet?
		// public string petOwner { get; } // un-settable since it is derived

		// Constructor
		public Pet(string name, string type)
		{
			petName = name;
			petType = type;
		}
	}

	public class PetOwnerGender
	{
		// Properties
		public string petName { get; set; } // probably the setter isn't relevant
		public string petOwnerGender { get; set; }

		// Constructor
		public PetOwnerGender(string name, string gender)
		{
			petName = name;
			petOwnerGender = gender;
		}
	}
}
